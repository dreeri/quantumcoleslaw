﻿using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlatformMovement : MonoBehaviour {


    public static float time;
	
	private Transform[] circles;
	private Vector3[]movementSpeedVectors;
	
	private float smoothTime = 0.1f;
	
	private Vector3 initial;
	// Use this for initialization
	
	void Awake(){
        Transform[] children = transform.GetComponentsInChildren<Transform>();
        List<Transform> directChildren = new List<Transform>();
        foreach (Transform child in children)
        {
            if (child.parent == transform)
                directChildren.Add(child);
        }

        circles = directChildren.ToArray();
		movementSpeedVectors = new Vector3[circles.Length];
		for(int i = 0; i < movementSpeedVectors.Length; i++) {
			movementSpeedVectors[i] = Vector3.zero;
		}
		
		initial = circles [0].position;
	}
	
	void Start () {
		
	}
	
	void Update(){
		for(int i = 0; i < circles.Length; i++){
			var movementVector = movementSpeedVectors[i];
			// loose animation circles [i].position = Vector3.SmoothDamp(circles [i].position, circles [i].position+2f*Mathf.Sin(i+Time.time*10) * new Vector3 (1, 0.6f, 1)-new Vector3 (0, 3.5f, 0), ref movementVector, smoothTime);
			// MUSIC FAST circles [i].position = Vector3.SmoothDamp(circles [i].position, initial+Mathf.Sin(i*4+Time.time*20) * new Vector3 (0, 0.4f, 0), ref movementVector, smoothTime);
            circles[i].position = Vector3.SmoothDamp(circles[i].position, initial + Mathf.Sin(i * 2 + time * 10) * new Vector3(0, 0.4f, 0), ref movementVector, smoothTime);
		}
	}
	
}
