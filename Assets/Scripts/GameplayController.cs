﻿using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class GameplayController : MonoBehaviour {

    public enum GameplayState
    {
        Playing,
        Failed,
        Completed,
        Paused
    }

    public GameplayState state;

    public Scene CurrentLevel { get; set; }

    public List<IResettable> ResettableObjects;
    public int Circles;

    // Effect to use when level is failed
    List<ColorCorrectionCurves> levelFailedImageEffects;
    private float levelFailedImageRefSpeed = 0;
    private float levelFailedImageSmoothTime = 1.0f;

    private float levelFailedTimeSmoothTime = 1.0f;
    private float levelFailedTimeRefSpeed = 0;

    private AudioEffectPlayer audioPlayer;

    void Awake()
    {
        Time.timeScale = 1f;
        audioPlayer = GameObject.FindGameObjectWithTag("AudioPlayer").GetComponent<AudioEffectPlayer>();
    }

    void Start()
    {
        levelFailedImageEffects = new List<ColorCorrectionCurves>();
        var cameras = GameObject.FindGameObjectsWithTag("MainCamera");
        foreach (var c in cameras)
        {
            var effect = c.GetComponent<ColorCorrectionCurves>();
            if (effect == null)
                effect = c.AddComponent<ColorCorrectionCurves>();
            levelFailedImageEffects.Add(effect);
        }
    }

    void Update()
    {
        if (state == GameplayState.Failed)
        {
            // Smooth damp camera torwards black-and-white colors
            foreach (var effect in levelFailedImageEffects)
            {
                effect.saturation = Mathf.SmoothDamp(effect.saturation, 0, ref levelFailedImageRefSpeed, levelFailedImageSmoothTime);
            }

            // Smooth damp time torwards zero
            Time.timeScale = Mathf.SmoothDamp(Time.timeScale, 0, ref levelFailedTimeRefSpeed, levelFailedImageSmoothTime);
        }
        if (state == GameplayState.Completed)
        {
            // Smooth damp time torwards zero
            Time.timeScale = Mathf.SmoothDamp(Time.timeScale, 0, ref levelFailedTimeRefSpeed, levelFailedImageSmoothTime);
            // Smooth damp camera torwards black-and-white colors
            foreach (var effect in levelFailedImageEffects)
            {
                effect.saturation = Mathf.SmoothDamp(effect.saturation, 5f, ref levelFailedImageRefSpeed, levelFailedImageSmoothTime);
            }
        }
    }
    
    public void OnLevelFailed()
    {
        audioPlayer.PlayDeath();
        state = GameplayState.Failed;
        //Time.timeScale = 0f;
        Debug.Log("Level failed!");
        ResetLevel(3f);
    }



    public void OnLevelCompleted()
    {
        if (state == GameplayState.Playing)
        {
            state = GameplayState.Completed;
            audioPlayer.PlayWin();
            StartCoroutine("LoadNextLevel",2.7f);           
            Debug.Log("Level completed!");
        }        
    }

    IEnumerator LoadNextLevel(float delay)
    {
        float startTime = Time.realtimeSinceStartup;
        while (startTime + delay > Time.realtimeSinceStartup)
        {
            yield return 0;
        }

        var nextLevel = Application.loadedLevel + 1;
        Debug.Log("Next level" + nextLevel);
        if (Application.CanStreamedLevelBeLoaded(nextLevel))
        {
            Application.LoadLevel(nextLevel);
        }
    }

    public void Pause()
    {
        Time.timeScale = 0f;
    }

    public void StartLevel()
    {
        state = GameplayState.Playing;
        Time.timeScale = 1f;
    }

    /// <summary>
    /// Reset game state
    /// </summary>
    public void ResetLevel()
    {
        if (ResettableObjects != null)
        {
            foreach (IResettable obj in ResettableObjects)
            {
                obj.Reset();
            }
            Debug.Log(string.Format("Resetted {0} gameobjects", ResettableObjects.Count));
        }

        //Reset image effects
        levelFailedImageRefSpeed = 0;
        levelFailedTimeRefSpeed = 0;
        foreach (var effect in levelFailedImageEffects)
        {
            effect.saturation = 1f;
        }
        StopAllCoroutines();
        StartLevel();
    }

    public void ResetLevel(float delay)
    {
        StartCoroutine("ResetLevelIEnumerator", delay);
    }


    private IEnumerator ResetLevelIEnumerator(float delay)
    {
        float startTime = Time.realtimeSinceStartup;
        while (startTime + delay > Time.realtimeSinceStartup)
        {
            yield return new WaitForEndOfFrame();
        }
        ResetLevel();
    }

    public void RegisterResettableObject(IResettable resettable)
    {
        if (ResettableObjects == null)
            ResettableObjects = new List<IResettable>();
        ResettableObjects.Add(resettable);
    }
}
