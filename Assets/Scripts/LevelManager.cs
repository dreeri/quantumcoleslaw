﻿using UnityEngine;
using System.Collections;

public static class LevelManager {

    public static void LoadScene(Scene scene)
    {
        Application.LoadLevel(scene.Name);
    }

    public static void LoadNextLevel(Scene level)
    {
        Application.LoadLevel(level.Index + 1);
    }

    public static bool IsLastLevel(Scene level)
    {
        // TODO: Replace with proper solution. This is just a quick way of checking if this is last level
        return level.Index == 5;
    }

}

public class Scene
{
    public Scene(string name, int index)
    {
        Name = name;
        Index = index;
    }

    public string Name{ get; set; }
    public int Index { get; set; }
}

public static class Scenes
{
    public static Scene Level1
    {
        get { return new Scene("level1", 0); }
    }

    public static Scene Level2
    {
        get { return new Scene("level2", 1); }
    }

    public static Scene Level3
    {
        get { return new Scene("level3", 2); }
    }

    public static Scene Level4
    {
        get { return new Scene("level4", 3); }
    }

    public static Scene Level5
    {
        get { return new Scene("level5", 4); }
    }

    public static Scene Level6
    {
        get { return new Scene("level6", 5); }
    }
}
