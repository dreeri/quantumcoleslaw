﻿using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlatformController : MonoBehaviour {


    private IEnumerable<Circle> circles { get; set; }

    /// <summary>
    /// Bpm of the music. Rotational speed of circles are multiplied by this value.
    /// </summary>
    public float Bpm;


    public void Initialize()
    {
        CacheCirles();

        // Multiply rotational speed with bpm to sync with music
        foreach (var circle in circles)
        {
            circle.circleSpeed *= Bpm;
        }
    }

    private void CacheCirles()
    {
        List<Circle> circleList = new List<Circle>();
        // Add circle components
        foreach (Transform child in transform)
        {
            var circle = child.GetComponent<Circle>();
            if (circle == null)
            {
                child.gameObject.AddComponent<Circle>();
            }
            circleList.Add(circle);
        }
    } 

}
