﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public Transform quantumWheel1;
	public Transform quantumWheel2;
	public float quantumWheelSpeed = 10.0f;
	
	float inputDampening = 0.2f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate()
	{
		// ManageInput ();
	}

	void ManageInput()
	{
		if (Input.GetAxis ("Horizontal") >= inputDampening) {
			quantumWheel1.transform.Rotate (Vector3.up * Time.deltaTime * quantumWheelSpeed);
			quantumWheel2.transform.Rotate (Vector3.up * Time.deltaTime * quantumWheelSpeed);
		}
	
		if (Input.GetAxis ("Horizontal") <= -inputDampening) {
			quantumWheel1.transform.Rotate (Vector3.down * Time.deltaTime * quantumWheelSpeed);
			quantumWheel2.transform.Rotate (Vector3.down * Time.deltaTime * quantumWheelSpeed);
		}
	}
}
