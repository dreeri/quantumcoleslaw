﻿using UnityEngine;
using System.Collections;

public class UvOffSet : MonoBehaviour {
	public float speed = 0.025f;
	public float offset;
	public int horizontal; //0 = horizontal movement, 1 = vertical movement
	public bool reversed;
	public float resetValue;
	// Use this for initialization
	void Start () {

	}
	// Update is called once per frame
	void Update () {
//		offset = offset + Time.deltaTime * speed;
//		//float startPos = 1 - offset;
//		renderer.material.SetTextureOffset ("_MainTex", new Vector2(1 - offset,0));
//		if (offset > 2) {
//			renderer.material.SetTextureOffset ("_MainTex", new Vector2(1f,0));
//			offset = 0;
//		}
		OffSetting ();
	}
	void OffSetting()
	{
        var renderer = GetComponent<Renderer>();

        switch (horizontal) {
			//horizontal
		case 0:
		{

			if(reversed)
			{
				offset = offset + Time.deltaTime * speed;
				resetValue = 1f;
			}
			else if(!reversed)
			{
				offset = offset - Time.deltaTime * speed;
				resetValue = -1f;
			}

                    //float startPos = 1 - offset;
                renderer.material.SetTextureOffset ("_MainTex", new Vector2(resetValue - offset,0));
			if (offset > 2 || offset < -2) {
				renderer.material.SetTextureOffset ("_MainTex", new Vector2(resetValue,0));
				offset = 0;
			}
		}
			break;
			//vertical
		case 1:
		{

			if(reversed)
			{
				offset = offset + Time.deltaTime * speed;
				resetValue = 1f;
			}
			else if(!reversed)
			{
				offset = offset - Time.deltaTime * speed;
				resetValue = -1f;
			}
			renderer.material.SetTextureOffset ("_MainTex", new Vector2(0,resetValue - offset));
			if (offset > 2 || offset < -2) {
				renderer.material.SetTextureOffset ("_MainTex", new Vector2(0,resetValue));
				offset = 0;
			}
		}
			break;
		}
	}
}