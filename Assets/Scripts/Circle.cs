﻿using UnityEngine;
using System.Collections;

public class Circle : MonoBehaviour, IResettable {

    public int Id { get; set; }

    public float circleSpeed;
    
    private Transform cachedTransform;
    private Quaternion originalRotation;
    private Vector3 originalPosition;

    private GameplayController gameplayController;

    void Awake()
    {
        cachedTransform = transform;
        originalPosition = cachedTransform.position;
        originalRotation = cachedTransform.rotation;
        gameplayController = GameObject.FindGameObjectWithTag("GameplayController").GetComponent<GameplayController>();
        gameplayController.RegisterResettableObject(this);
    }
    	
	// Update is called once per frame
	void Update () {
        cachedTransform.Rotate(Vector3.forward, circleSpeed * Time.deltaTime);
	}

    public void Reset()
    {
        cachedTransform.position = originalPosition;
        cachedTransform.rotation = originalRotation;
    }
}


public interface IResettable
{
    void Reset();
}
    
