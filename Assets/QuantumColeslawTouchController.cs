﻿using UnityEngine;
using System;
using System.Collections;
using TouchScript.Gestures;

public class QuantumColeslawTouchController : MonoBehaviour {


    private ParticleController particleController;

	// Use this for initialization
	void Start () {
	    
	}

    private void OnEnable()
    {
        GetComponent<FlickGesture>().Flicked += flickedHandler;
    }

    private void OnDisable()
    {
        GetComponent<FlickGesture>().Flicked -= flickedHandler;
    }
    
    public void flickedHandler(object sender, EventArgs args)
    {

    }
}
