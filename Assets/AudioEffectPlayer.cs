﻿using UnityEngine;
using System.Collections;

public class AudioEffectPlayer : MonoBehaviour {

    public AudioSource upSource;
    public AudioSource downSource;
    public AudioSource deathSource;
    public AudioSource winSource;

    public void PlayUpMovement()
    { 
        upSource.Play();
    }

    public void PlayDownMovement()
    {
        downSource.Play();
    }

    public void PlayDeath()
    {
        deathSource.Play();
    }

    public void PlayWin()
    {
        winSource.Play();
    }
}
