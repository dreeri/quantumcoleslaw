﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Idea: Particle is always facing to the center of the circle and keys A,D move particle sideways along the circle
/// </summary>
public class ParticleController : MonoBehaviour, IResettable {

    private Transform cachedTransform;

    /// <summary>
    /// 7 layers in total, start outside circles
    /// </summary>
    public int currentLayer;

    public int amountOfCircles;

    // Quick solution of move player from one circle to another
    public float CircleWidth = 1f;

	public float GapWidth = 0.025f;

    //
    private float radialMovementSpeed;
    private float refMovementSpeed;

    private float lastAngle;

    public float maximumRadialSpeed = 10f;
    public float radialMovementSpeedFactor = 0.1f;

    public bool loadGlobalSettingsAtStart = true;
    private GameplayController gameplayController;

    private Camera camera;
    private Vector3 camInitialPosition;
    private Quaternion camInitialRotation;
    private Transform cachedCameraTransform;

    private AudioEffectPlayer audioPlayer;

    /// <summary>
    /// Time which it takes for particle sideways movement to stop if no key is pressed
    /// </summary>
    public float smoothTime = 1f;

    private Vector3 initialPosition;
    private Quaternion initialRotation;

    private bool enableInput = true;

    void Awake()
    {
        cachedTransform = transform;
        initialPosition = cachedTransform.position;
        initialRotation = cachedTransform.rotation;

        camera = GetComponentInChildren<Camera>();
        cachedCameraTransform = camera.transform;
        camInitialPosition = cachedCameraTransform.position;
        camInitialRotation = cachedCameraTransform.rotation;

        audioPlayer = GameObject.FindGameObjectWithTag("AudioPlayer").GetComponent<AudioEffectPlayer>();

        var gpcGo = GameObject.FindGameObjectWithTag("GameplayController");
        if (gpcGo != null)
        {
            gameplayController = gpcGo.GetComponent<GameplayController>();
            currentLayer = gameplayController.Circles;
            amountOfCircles = gameplayController.Circles;
            gameplayController.RegisterResettableObject(this);
        }

        if (loadGlobalSettingsAtStart)
        {
            maximumRadialSpeed = ParticleControllerSettings.maxiumumRadialSpeed;
            radialMovementSpeedFactor = ParticleControllerSettings.radialMovementSpeed;
            smoothTime = ParticleControllerSettings.smoothTime;
        }
    }

    void Update()
    {
        if (enableInput)
        {
            // Movement from one circle to another
            if ((Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) && currentLayer > 0)
            {
                currentLayer--;
                audioPlayer.PlayUpMovement();
            }

            else if ((Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) && currentLayer < (amountOfCircles))
            {
                currentLayer++;
                audioPlayer.PlayDownMovement();
            }

            //Radial movement
            if ((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) && radialMovementSpeed > -maximumRadialSpeed)
            {
                var newSpeed = radialMovementSpeed - radialMovementSpeedFactor;
                radialMovementSpeed = Mathf.SmoothDamp(radialMovementSpeed, newSpeed, ref refMovementSpeed, smoothTime);
            }

            else if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && radialMovementSpeed < maximumRadialSpeed)
            {
                var newSpeed = radialMovementSpeed + radialMovementSpeedFactor;
                radialMovementSpeed = Mathf.SmoothDamp(radialMovementSpeed, newSpeed, ref refMovementSpeed, smoothTime);
            }

            else
            {
                // If no sideways movement is applied we slow down the particle
                radialMovementSpeed = Mathf.SmoothDamp(radialMovementSpeed, 0, ref refMovementSpeed, smoothTime);
            }
        }
        else
        {
            // If no sideways movement is applied we slow down the particle
            radialMovementSpeed = Mathf.SmoothDamp(radialMovementSpeed, 0, ref refMovementSpeed, smoothTime);
        }
            
        
        
        ApplyRadialMotion();
       
        //Game won
        if (currentLayer == 0)
            gameplayController.OnLevelCompleted();

        if (gameplayController.state == GameplayController.GameplayState.Completed || gameplayController.state == GameplayController.GameplayState.Failed)
            enableInput = false;

    }

    public bool TryIncreaseCurrentCircle()
    {
        if ( currentLayer < (amountOfCircles))
        {
            currentLayer++;
            return true;
        }
        return false;
    }

    public bool TryDecreaseCurrentCircle()
    {
        if (currentLayer > 0)
        {
            currentLayer--;
            return true;
        }
        return false;
    }

    private void ApplyRadialMotion()
    {
        float radius;
        if (currentLayer == 0){
            radius = 0;
        }
            
        else
            radius = (CircleWidth + GapWidth) * (currentLayer + 0.5f);
        var angle = (radialMovementSpeed * Time.deltaTime) + lastAngle;
        lastAngle = angle;
        var x = Mathf.Cos(angle) * radius;
        var z = Mathf.Sin(angle) * radius;
        cachedTransform.localPosition = new Vector3(x, cachedTransform.localPosition.y, z);
        cachedTransform.LookAt(transform.parent.position);

        if (currentLayer == 0)
        {
            cachedCameraTransform.position = cachedTransform.position + new Vector3(0, 3, 0);
            cachedCameraTransform.LookAt(cachedTransform);
        }
    }

    void OnCollisionEnter(Collision collision) {
        gameplayController.OnLevelFailed();
    }

    void OnTriggerEnter(Collider collider)
    {
        gameplayController.OnLevelFailed();
    }

    public void Reset()
    {
        radialMovementSpeed = 0;
        refMovementSpeed = 0;
        lastAngle = 0;
        cachedTransform.position = initialPosition;
        cachedTransform.rotation = initialRotation;
        currentLayer = gameplayController.Circles;
        enableInput = true;
        //Reset cam
        cachedCameraTransform.position = camInitialPosition;
        cachedCameraTransform.rotation = camInitialRotation;
    }
}

public static class ParticleControllerSettings
{
    public static float smoothTime = 0.1f;
    public static float maxiumumRadialSpeed = 7.0f;
    public static float radialMovementSpeed = 1.0f;
}